//-------------------------------------------
//
// Dung D. Phan <brianp.dung@utexas.edu>
// https://gitlab.com/brianp.dung
//
// Created: 2019-03-29 23:32:51
// Description: NOvA TB signal generator
//
//-------------------------------------------

#pragma once

#include <iostream>
#include <iomanip>
#include <vector>
#include <map>
#include <cstring>
#include <fstream>
#include <algorithm>
#include <utility>
#include <ctime>

#include <TROOT.h>
#include <TH1.h>
#include <TH2.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TStyle.h>
#include <TTree.h>
#include <TFile.h>
#include <TMath.h>
#include <TRandom1.h>

const unsigned int maxHits = 100;
const unsigned int nSamples = 1024;
const double       eCharge = 1.6E-19;

typedef struct {
    unsigned int    totalNHits;
    unsigned int    triggerNHits;
    unsigned int	waveform      [nSamples];
    unsigned int    npeOfHits     [maxHits];
    unsigned int    timeOfHitsInNS[maxHits];
} TOF_t;

class SignalGenerator {
public:
    SignalGenerator();
    virtual ~SignalGenerator();

    void GenerateWaveform(TOF_t &tofStruct);

    void SetNSamples(unsigned int nsamples);
    void SetMaxNHits(unsigned int maxHits);
    void SetSamplingRateInGSPS(double samplingRateInGSPS);
    void SetMainSignal(double triggerTimeInNanoSec, double mainNPE);
    void SetMainSignalFailRate(double triggerSignalFailRate);
    void SetSignalRiseTimeInNS(double riseTimeInNS);
    void SetSignalFallTimeInNS(double fallTimeInNS);
    void SetTimeWalkRate(double timeWalkRate);
    void SetGain(double pmtGain);
    void SetDarkCurrentInHz(double pmtDarkCurrentRateInHz);
    void SetWhiteNoiseInVolts(double rmsNoiseAmplitudeInVolt);
    void SetTriggerTimeSpreadInNS(double triggerTimeSpreadInNS);
    void Setup();

private:
    void Go();
    void GenerateNoise();
    void GenerateHits();
    void GenerateDarkCurrent();
    void GenerateTriggerSignal();
    void DigitizeSignal();
    double PMTSinglePEResponse(double ticktime, double delay, double effectiveRiseTime, double effectiveFallTime);
    double PMTMultiplePEResponse(double ticktime, double delay, unsigned int npe);
    void Reset(TOF_t &tofStruct);

private:
    unsigned int                 _DigitizerNSamples;
    unsigned int                 _MaxNHits;
    unsigned int                 _DigitizerOffset;

    unsigned int                 _TotalNumberOfHits;
    std::vector<double>          _PMTResponseInVolt;
    std::vector<int>             _Waveform;
    std::vector<unsigned int>    _NPEOfHits;
    std::vector<double>          _TimeOfHitsInNS;

    TRandom1*                    _RndGenerator;
    std::vector<double>          _WhiteNoiseInADC;
    double                       _RMSNoiseAmplitudeInVolt;

    double                       _PMTGain;
    double                       _SinglePhotoelectronAmplitudeInVolt;

    double                       _PMTDarkCurrentRateInHz;
    unsigned int                 _NDarkCurrentHits;
    double                       _MeanNDarkCurrentHits;

    double                       _TriggerTimeSpreadInNS;
    unsigned int                 _NTriggerHits;
    double                       _MeanNTriggerHits;
    bool                         _UseTriggerSignal;
    double                       _TriggerTimeInNS;
    double                       _TriggerNPE;
    double                       _TriggerSignalFailRate;
    double                       _RiseTimeInNS;
    double                       _FallTimeInNS;
    double                       _TimeWalkRate;

    double                       _SamplingRateInGSPS;  
    double                       _DigitizerTimeResolutionInNS;
    double                       _DigitizerWindowInNS;
};
