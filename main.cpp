#include <TROOT.h>
#include <TH1.h>
#include <TH2.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TStyle.h>
#include <TTree.h>
#include <TFile.h>
#include <TMath.h>

#include <iostream>
#include <iomanip>
#include <vector>
#include <map>
#include <cstring>
#include <fstream>
#include <algorithm>
#include <utility>

#include <SignalGenerator.h>

int main() {

    double triggerTimeInNS = 50;
    double mainNPE = 50;
    double triggerSignalFailRate = 0.1;
    double samplingRateInGSPS = 2.5E9;
    double riseTimeInNS = 1;    // Rise time for SPE
    double fallTimeInNS = 3;    // Fall time for SPE
    double timeWalkRate = 0.05; // Rise/fall time increases by 5% every PE added.
    double triggerTimeSpreadInNS = 1;
    double pmtGain = 2E6;
    double pmtDarkCurrentRateInHz = 1E2;
    double rmsNoiseAmplitudeInVolt = 0.488E-3;

//    TFile *aFile = new TFile("ToF_Simulation.root", "RECREATE");
    TFile *aFile = new TFile(Form("/Users/dphan/Research/novatb/tof-analysis/ToF_Simulation_%i_2d5GSs.root", (int)mainNPE), "RECREATE");
    TTree *TOF = new TTree("TOF", "TOF Waveforms");
    TOF_t tof;

    TOF->Branch("totalNHits", &(tof.totalNHits), "totalNHits/I");
    TOF->Branch("triggerNHits", &(tof.triggerNHits), "triggerNHits/I");
    TOF->Branch("waveform", tof.waveform, "waveform[1024]/I");
    TOF->Branch("npeOfHits", tof.npeOfHits, "npeOfHits[totalNHits]/I");
    TOF->Branch("timeOfHitsInNS", tof.timeOfHitsInNS, "timeOfHitsInNS[totalNHits]/I");

    SignalGenerator *sigGen = new SignalGenerator();

    sigGen->SetNSamples(nSamples);
    // Number of sampling points

    sigGen->SetMaxNHits(maxHits);

    sigGen->SetSamplingRateInGSPS(samplingRateInGSPS);
    // Sampling at 5GS/s

    sigGen->SetMainSignal(triggerTimeInNS, mainNPE);
    // Optional (false as default)
    // Call this function if want to have a main trigger signal at the trigger time
    // Ignore this function if want to have only dark current

    sigGen->SetMainSignalFailRate(triggerSignalFailRate);
    // Optional (no fail rate as default)
    // Set the rate at which the trigger signal failed to be generated (leaving only dark current in the waveform)

    sigGen->SetSignalRiseTimeInNS(riseTimeInNS);
    sigGen->SetSignalFallTimeInNS(fallTimeInNS);
    sigGen->SetTimeWalkRate(timeWalkRate);
    // Set rise/fall time

    sigGen->SetGain(pmtGain);
    // Set gain for signal
    // Basically, this gain translates the single PE response to the pulse amplitude of the PMT/SiPM

    sigGen->SetDarkCurrentInHz(pmtDarkCurrentRateInHz);
    // Set the dark current rate
    // Optional for a realistic simulation

    sigGen->SetWhiteNoiseInVolts(rmsNoiseAmplitudeInVolt);
    // Using Gaussian white noise

    sigGen->SetTriggerTimeSpreadInNS(triggerTimeSpreadInNS);
    // Set Transit Time Spread for SiPM/PMT.

    sigGen->Setup();

    unsigned int NEvents = 20000;
    for (unsigned int i = 0; i < NEvents; i++) {
        
        sigGen->GenerateWaveform(tof);
        // Generate waveform and buffer info in tof structure

        TOF->Fill();
    }

    TOF->Write(0, TObject::kOverwrite);
    aFile->Write(0, TObject::kOverwrite);
    aFile->Close();

    return 0;
}