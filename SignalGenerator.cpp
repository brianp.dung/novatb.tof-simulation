//-------------------------------------------
//
// Dung D. Phan <brianp.dung@utexas.edu>
// https://gitlab.com/brianp.dung
//
// Created: 2019-03-29 23:31:17
// Description: NOvA TB signal generator
//
//-------------------------------------------

#include <SignalGenerator.h>
#include <CoreServices/CoreServices.h>

SignalGenerator::SignalGenerator() : _UseTriggerSignal(false) {
    _RndGenerator = new TRandom1();
    _RndGenerator->SetSeed(std::time(0));
}

SignalGenerator::~SignalGenerator() = default;

void SignalGenerator::SetNSamples(unsigned int nsamples) {
    _DigitizerNSamples = nsamples;
}

void SignalGenerator::SetSamplingRateInGSPS(double samplingRateInGSPS) {
    _SamplingRateInGSPS = samplingRateInGSPS;
}

void SignalGenerator::SetMainSignal(double triggerTimeInNanoSec, double mainNPE) {
    _UseTriggerSignal = true;
    _TriggerTimeInNS = triggerTimeInNanoSec;
    _TriggerNPE = mainNPE;
}

void SignalGenerator::SetMainSignalFailRate(double triggerSignalFailRate) {
    _TriggerSignalFailRate = triggerSignalFailRate;
}

void SignalGenerator::SetSignalRiseTimeInNS(double riseTimeInNS) {
    _RiseTimeInNS = riseTimeInNS;
}

void SignalGenerator::SetSignalFallTimeInNS(double fallTimeInNS) {
    _FallTimeInNS = fallTimeInNS;
}

void SignalGenerator::SetTimeWalkRate(double timeWalkRate) {
    _TimeWalkRate = timeWalkRate;
}

void SignalGenerator::SetGain(double pmtGain) {
    _PMTGain = pmtGain;
}

void SignalGenerator::SetDarkCurrentInHz(double pmtDarkCurrentRateInHz) {
    _PMTDarkCurrentRateInHz = pmtDarkCurrentRateInHz;
}

void SignalGenerator::SetWhiteNoiseInVolts(double rmsNoiseAmplitudeInVolt) {
    _RMSNoiseAmplitudeInVolt = rmsNoiseAmplitudeInVolt;
}

void SignalGenerator::SetMaxNHits(unsigned int maxHits) {
    _MaxNHits = maxHits;
}

void SignalGenerator::Reset(TOF_t &tofStruct) {
    _TotalNumberOfHits = 0;
    _NDarkCurrentHits = 0;
    _NTriggerHits = 0;

    _Waveform.clear();
    _NPEOfHits.clear();
    _TimeOfHitsInNS.clear();
    _WhiteNoiseInADC.clear();
    _PMTResponseInVolt.clear();


    tofStruct.totalNHits = 0;

    for (unsigned int i = 0; i < _DigitizerNSamples; i++) {
        tofStruct.waveform[i] = 0;
    }

    for (unsigned int i = 0; i < _MaxNHits; i++) {
        tofStruct.npeOfHits[i] = 0;
        tofStruct.timeOfHitsInNS[i] = 0;
    }
}

void SignalGenerator::GenerateWaveform(TOF_t &tofStruct) {
    Reset(tofStruct);
    Go();

    tofStruct.totalNHits = _TotalNumberOfHits;
    tofStruct.triggerNHits = _NTriggerHits;

    for (unsigned int i = 0; i < _TotalNumberOfHits; i++) {
        tofStruct.npeOfHits[i] = _NPEOfHits.at(i);
        tofStruct.timeOfHitsInNS[i] = _TimeOfHitsInNS.at(i);
    }

    for (unsigned int i = 0; i < _DigitizerNSamples; i++) {
        tofStruct.waveform[i] = _Waveform.at(i);
    }
}

void SignalGenerator::Go() {
    GenerateNoise();
    GenerateHits();
}

void SignalGenerator::GenerateNoise() {
    for (unsigned int i = 0; i < _DigitizerNSamples; i++) {
        _WhiteNoiseInADC.push_back((_RndGenerator->Gaus(0., _RMSNoiseAmplitudeInVolt)) * 4096);
    }
}

void SignalGenerator::Setup() {
    _DigitizerTimeResolutionInNS = 1E9 / _SamplingRateInGSPS;
    _DigitizerWindowInNS = _DigitizerTimeResolutionInNS * (double) _DigitizerNSamples;
    _MeanNDarkCurrentHits = _DigitizerWindowInNS * _PMTDarkCurrentRateInHz * 1E-9;
    _MeanNTriggerHits = _UseTriggerSignal ? 1. : 0.;
    _DigitizerOffset = 4000;
}

double
SignalGenerator::PMTSinglePEResponse(double ticktime, double delay, double effectiveRiseTime, double effectiveFallTime) {
    double shape = ticktime > delay ? TMath::Exp(-(ticktime - delay) / effectiveFallTime) *
                                  (1 - TMath::Exp(-(ticktime - delay) / effectiveRiseTime)) : 0;

    double singlePECharge = _PMTGain * eCharge;
    _SinglePhotoelectronAmplitudeInVolt = (singlePECharge * (effectiveRiseTime + effectiveFallTime) * 50 * 1E9) /
                                          (effectiveFallTime * effectiveFallTime);

    return -_SinglePhotoelectronAmplitudeInVolt * shape;
}

double SignalGenerator::PMTMultiplePEResponse(double ticktime, double delay, unsigned int npe) {
    double response = 0.;
    double effectiveFallTime = _FallTimeInNS * (1 + npe * _TimeWalkRate);
    double effectiveRiseTime = _RiseTimeInNS * (1 + npe * _TimeWalkRate);
    for (unsigned int i = 0; i < npe; i++) {
        response += PMTSinglePEResponse(ticktime, delay, effectiveRiseTime, effectiveFallTime);
    }
    return response;
}

void SignalGenerator::GenerateHits() {
    GenerateDarkCurrent();

    if (_UseTriggerSignal) {
        GenerateTriggerSignal();
    }

    _TotalNumberOfHits = _NDarkCurrentHits + _NTriggerHits;

    DigitizeSignal();
}

void SignalGenerator::GenerateDarkCurrent() {
    _NDarkCurrentHits = _RndGenerator->Poisson(_MeanNDarkCurrentHits);

    for (unsigned int i = 0; i < _NDarkCurrentHits; i++) {
        _NPEOfHits.push_back(_RndGenerator->Poisson(1));
        _TimeOfHitsInNS.push_back(0.);

        if (_NPEOfHits.at(i) == 0) _NPEOfHits.at(i) = _RndGenerator->Poisson(1);

        // Keep sampling until there are no hit ticktime duplication
        bool noHitTimeDuplicated = true;
        do {
            _TimeOfHitsInNS.at(i) = _RndGenerator->Uniform(_DigitizerWindowInNS);
            for (unsigned int j = 0; j < i; j++) {
                noHitTimeDuplicated = noHitTimeDuplicated && (_TimeOfHitsInNS.at(j) != _TimeOfHitsInNS.at(i));
            }
            if (_UseTriggerSignal)
                noHitTimeDuplicated = noHitTimeDuplicated && (_TimeOfHitsInNS.at(i) != _TriggerTimeInNS);
        } while (!noHitTimeDuplicated);
    }

    for (unsigned int i = 0; i < _DigitizerNSamples; i++) {
        double voltage = 0.;
        _PMTResponseInVolt.push_back(0.);
        double ticktime = (double) i * _DigitizerTimeResolutionInNS;
        for (unsigned int j = 0; j < _NDarkCurrentHits; j++) {
            voltage += PMTMultiplePEResponse(ticktime, _TimeOfHitsInNS.at(j), _NPEOfHits.at(j));
        }
        _PMTResponseInVolt.at(i) += voltage;
    }
}

void SignalGenerator::GenerateTriggerSignal() {
    _NTriggerHits = _RndGenerator->Uniform(1) > _TriggerSignalFailRate ? (unsigned int) _MeanNTriggerHits : 0;
    for (unsigned int i = 0; i < _NTriggerHits; i++) {
        _NPEOfHits.push_back(_RndGenerator->Poisson(_TriggerNPE));
        _TimeOfHitsInNS.push_back(0.);
    }

    std::vector<std::vector<double>> triggerDelays;
    std::vector<double> tmp;
    for (unsigned int i = _NDarkCurrentHits; i < _NDarkCurrentHits + _NTriggerHits; i++) {
        if (_NPEOfHits.at(i) == 0) _NPEOfHits.at(i) = _RndGenerator->Poisson(_TriggerNPE);
        _TimeOfHitsInNS.at(i) = _TriggerTimeInNS;
        for (unsigned int k = 0; k < _NPEOfHits.at(i); k++) {
            tmp.push_back(_RndGenerator->Gaus(_TriggerTimeInNS, _TriggerTimeSpreadInNS));
        }
        triggerDelays.push_back(tmp);
    }

    for (unsigned int i = 0; i < _DigitizerNSamples; i++) {
        double voltage = 0.;
        double ticktime = (double) i * _DigitizerTimeResolutionInNS;
        for (unsigned int j = _NDarkCurrentHits; j < _NDarkCurrentHits + _NTriggerHits; j++) {
            for (unsigned int k = 0; k < _NPEOfHits.at(j); k++) {
                voltage += PMTMultiplePEResponse(ticktime, (triggerDelays.at(j - _NDarkCurrentHits)).at(k), 1);
            }
        }
        _PMTResponseInVolt.at(i) += voltage;
    }
}

void SignalGenerator::DigitizeSignal() {
    for (unsigned int i = 0; i < _DigitizerNSamples; i++) {
        _Waveform.push_back(_PMTResponseInVolt.at(i) * 4096);
        _Waveform.at(i) += _WhiteNoiseInADC.at(i);
        _Waveform.at(i) = TMath::Nint(_Waveform.at(i)) + _DigitizerOffset;
    }
}

void SignalGenerator::SetTriggerTimeSpreadInNS(double triggerTimeSpreadInNS) {
    _TriggerTimeSpreadInNS = triggerTimeSpreadInNS;
}









